/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas_enlazadas_2;

/**
 *
 * @author Jose
 */
public class Listas_Enlazadas_2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    
        Lista2 listaenlazada2 = new Lista2();
        Lista listaenlazada = new Lista();
        Lista1 listaenlazada1 = new Lista1();
        
         System.out.println("JOSE MARIA ANDRES UC MAY     MATRICULA: 63829");
        
        System.out.println("-----------------------------Primer_Ejercicio----------------------------------------");
        
        System.out.println("Cree una Lista enlazada que almacene un dato de "
                 + "tipo String, y guarde los nombres de 10 compañeros de la "
                 + "clase de Estructura de Datos.");
         
        listaenlazada2.agregarAlInicio2("Frida M Bacab Ic");
        listaenlazada2.agregarAlInicio2("Cristian Berzunza Poot");
        listaenlazada2.agregarAlInicio2("Raul A Caballero Ramirez");
        listaenlazada2.agregarAlInicio2("Orlando A Cach Laines");
        listaenlazada2.agregarAlInicio2("Darlin E Cahuich Ake");
        listaenlazada2.agregarAlInicio2("Gustavo M Castillo Canul");
        listaenlazada2.agregarAlInicio2("Daniel A Colli Aguilar");
        listaenlazada2.agregarAlInicio2("Gustavo M Castillo Canul");
        listaenlazada2.agregarAlInicio2("Alexander I De La Cruz Santos");
        listaenlazada2.agregarAlInicio2("Eduardo I Duarte Hernandez");
        
        listaenlazada2.mostrarListaEnlazada2();
        listaenlazada2.borrarDelInicio2();
        listaenlazada2.mostrarListaEnlazada2();
        listaenlazada2.borrarDelInicio2();
        listaenlazada2.borrarDelInicio2();
        listaenlazada2.mostrarListaEnlazada2();

        listaenlazada2.borrarDelInicio2();
        listaenlazada2.borrarDelInicio2();
        listaenlazada2.mostrarListaEnlazada2();
        
        System.out.println("-----------------------------Segundo_Ejercicio---------------------------------------");
        
        System.out.println("Cree una Lista enlazada que almacene dos datos uno "
                + "de tipo entero, y otro de tipo String, y guarde los nombres"
                + " y la edad  de 10 compañeros de la clase de Estructura de"
                + " Datos");
        
        listaenlazada1.agregarAlInicio("Frida M Bacab Ic");
        listaenlazada1.agregarAlInicio("Cristian Berzunza Poot");
        listaenlazada1.agregarAlInicio("Raul A Caballero Ramirez");
        listaenlazada1.agregarAlInicio("Orlando A Cach Laines");
        listaenlazada1.agregarAlInicio("Darlin E Cahuich Ake");
        listaenlazada1.agregarAlInicio("Gustavo M Castillo Canul");
         listaenlazada1.agregarAlInicio("Daniel A Colli Aguilar");
        listaenlazada1.agregarAlInicio("Gustavo M Castillo Canul");
        listaenlazada1.agregarAlInicio("Alexander I De La Cruz Santos");
        listaenlazada1.agregarAlInicio("Eduardo I Duarte Hernandez");
        
        listaenlazada.agregarAlInicio(20);
        listaenlazada.agregarAlInicio(19);
        listaenlazada.agregarAlInicio(20);
        listaenlazada.agregarAlInicio(19);
        listaenlazada.agregarAlInicio(21);
        listaenlazada.agregarAlInicio(19);
        listaenlazada.agregarAlInicio(21);
        listaenlazada.agregarAlInicio(20);
        listaenlazada.agregarAlInicio(21);
        listaenlazada.agregarAlInicio(21);
        
        listaenlazada1.mostrarListaEnlazada();
        listaenlazada1.borrarDelInicio();
        listaenlazada1.mostrarListaEnlazada();
        listaenlazada1.borrarDelInicio();
        listaenlazada1.borrarDelInicio();
        listaenlazada1.mostrarListaEnlazada();

        listaenlazada1.borrarDelInicio();
        listaenlazada1.borrarDelInicio();
        listaenlazada1.mostrarListaEnlazada();
        
         listaenlazada.mostrarListaEnlazada();
        listaenlazada.borrarDelInicio();
        listaenlazada.mostrarListaEnlazada();
        listaenlazada.borrarDelInicio();
        listaenlazada.borrarDelInicio();
        listaenlazada.mostrarListaEnlazada();

        listaenlazada.borrarDelInicio();
        listaenlazada.borrarDelInicio();
        listaenlazada.mostrarListaEnlazada();
     
    }
    
}
